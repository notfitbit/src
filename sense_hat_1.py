import sys
from sense_hat import SenseHat

sense = SenseHat()


def main():
 
 
 
 if len(sys.argv) > 1:
  msg = sys.argv[1]
  sense.show_message(msg, text_colour=[255, 255, 255])
 t = sense.get_temperature()
 t = round(t, 2)
 p = sense.get_pressure()
 p = round(p, 2)
 h = sense.get_humidity()
 h = round(h, 2)
 print t,p,h

	    
	    
if __name__ == "__main__":
 main()
