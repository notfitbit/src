from sense_hat import SenseHat

sense = SenseHat()
count = 0

red = (255, 0, 0)

while True:
        acceleration = sense.get_accelerometer_raw()
        x = acceleration['x']
        y = acceleration['y']
        z = acceleration['z']

        x = abs(x)
        y = abs(y)
        z = abs(z)

        c = str(count)
        if x > 1 or y > 1 or z > 1:
                count+=1
                sense.show_message(c)

        else:
                sense.clear()




